import sys
from os.path import isfile

from fastai.imports import *
from fastai.transforms import *
from fastai.conv_learner import *
from fastai.model import *
from fastai.dataset import *
from fastai.sgdr import *
from fastai.plots import *

import warnings
warnings.filterwarnings("ignore")

args = sys.argv
device = int(args[1])
#detailed = int(args[2])
crop = args[2]
net = args[3]

torch.cuda.set_device(device)

crops = {
         "n":CropType.NO,        #good
         "r":CropType.RANDOM,    #normal
         "g":CropType.GOOGLENET  #bad
        }

archs = {"resnext101":resnext101, 
         "resnext101_64": resnext101_64,
         "wrn": wrn,
         "vgg19": vgg19,
         "resnet152": resnet152,
         "dn201": dn201
       }

bs_256 = {"resnext101": 30, 
          "resnext101_64": 20,
          "wrn": 25,
          "vgg19": 32,
          "resnet152": 36,
          "dn201": 34
         }

def species(n, detailed=True):
    if detailed:
        return n
    if n<6: #apple
        return 0
    if n<9: #cherry
        return 1
    if n<17: #corn
        return 2
    if n<24: #grape
        return 3
    if n<27: #citrus
        return 4
    if n<30: #peach
        return 5
    if n<33: #pepper
        return 6
    if n<38: #potato
        return 7
    if n<41: #strawberry
        return 8
    if n<61: #Tomato
        return 9
    assert False

PATH = "/mnt/ssd2tb/Plant_Disease_Recognition/input/"
sz = 256 
bs = bs_256[net]

train = "train/"
valid = "valid/"
test = "test/"

for FOLD in range(5):
    fnames = []
    y = []

    dir = "train"
    with open(PATH+f"ai_challenger_pdr2018_{dir}_annotations_20181021.json") as f:
        lines = f.readlines()

    for x in json.loads(lines[0]):
        fnames.append(train+x['image_id'])
        y.append(species(x['disease_class']))

    dir = "validation"
    with open(PATH+f"ai_challenger_pdr2018_{dir}_annotations_20181021.json") as f:
        lines = f.readlines()

    for x in json.loads(lines[0]):
        fnames.append(valid+x['image_id'])
        y.append(species(x['disease_class']))
    
    arch = archs[net] 
    name = net+"_"+str(sz)+"_"+str(bs)+"_"+crop+"_"+str(FOLD)
    
    val_idxs = get_cv_idxs(len(y), cv_idx=FOLD, val_pct=0.2, seed=57)
    print(name, len(y), len(val_idxs))
    tfms = tfms_from_model(arch, sz, aug_tfms=transforms_side_on, max_zoom=1.2, crop_type = crops[crop])

    data = ImageClassifierData.from_names_and_array(PATH, tfms=tfms, bs=bs, fnames=fnames, 
                                                    y=np.array(y), val_idxs=val_idxs, 
                                                    test_name=test, classes=sorted(list(set(y))))
    learn = ConvLearner.pretrained(arch, data, precompute=False)
    pd.DataFrame({"path":learn.data.val_dl.dataset.fnames}).to_csv(PATH+f"{FOLD}_idx.csv", index=False)
    
    print("fit lastlayer...")
    for i in range(2):
        if isfile(PATH+"/models/"+f'{name}_lastlayer_{i}.h5'):
            learn.load(f'{name}_lastlayer_{i}')
            print(f"{FOLD} skip_lastlayer_{i}")
        else:
            learn.fit(1e-2, 1, cycle_len=1)
            learn.save(f'{name}_lastlayer_{i}')
            with open(PATH.replace("input","output")+f"log_{name}.txt", "a") as f:
                log = f"{FOLD} lastlayer_{i} " + str(learn.sched.val_losses[0]) + " " + str(learn.sched.rec_metrics[0]) 
                f.write(log + "\n")
            
    learn.unfreeze()

    lr=np.array([1e-4,1e-3,1e-2])

    print("fit all layers...")
    for i in range(5):
        if isfile(PATH+"/models/"+f'{name}_all_{i}.h5'):
            learn.load(f'{name}_all_{i}')
            print(f"{FOLD} skip_all_{i}")
        else:
            learn.fit(lr, 1, cycle_len=1, cycle_mult=1, use_clr = (10,10,0.95,0.85))
            learn.save(f'{name}_all_{i}')
            with open(PATH.replace("input","output")+f"log_{name}.txt", "a") as f:
                log = f"{FOLD} all_{i} " + str(learn.sched.val_losses[0]) + " " + str(learn.sched.rec_metrics[0]) 
                f.write(log + "\n")

    print("predict test...")
    file1 = PATH.replace("input","output")+f"submit_{name}.json"
    file2 = PATH.replace("input","output")+f"test_{name}.csv"
    if not isfile(file1) or not isfile(file2):

        fnames = [c.split("/")[-1] for c in learn.data.test_dl.dataset.fnames]
        log_preds = learn.TTA(n_aug=4, is_test=True)
        probs = np.mean(np.exp(log_preds[0]),0)
        ans = np.argmax(probs, axis=1)
        
        sub = []
        for x,y in zip(fnames, ans):
            sub.append({
                            "image_id": x,
                            "disease_class":int(y) 
                        })

        with open(file1, 'w') as outfile:
            json.dump(sub, outfile)
            
        df = pd.DataFrame({"id":fnames})
        for i in range(61):
            df[i] = probs[:,i]
        df.to_csv(file2, index=False)

    print("predict valid...")
    file0 = PATH.replace("input","output")+f"valid_{name}.csv"
    if not isfile(file0):
        
        fnames = [c.split("/")[-1] for c in learn.data.val_dl.dataset.fnames]
        log_preds = learn.TTA(n_aug=4)
        probs = np.mean(np.exp(log_preds[0]),0)
        ans = np.argmax(probs, axis=1)

        df = pd.DataFrame({"id":fnames})
        for i in range(61):
            df[i] = probs[:,i]
        df.to_csv(file0, index=False)

    print("finish")
